<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function getOrders(){
        if(\Auth::check() && \Auth::getUser()->getAttribute('admin')==1){
            $orders = \DB::table('order')->where('status','=',0)->orWhere('lunas','=',0)->get();
            return view('admin.home',compact('orders'));
        }
        else{
            return redirect('home');
        }
    }

    public function getShow($nis){
        $siswa = \DB::table('t_siswa')->where('nis',$nis)->first();
        $siswa_tingkat = \DB::table('t_siswa_tingkat')->where('nis',$siswa->nis)->first();
        $absences = \DB::table('t_kesiangan')->where('nis',$siswa->nis)->get();

        $sumMenit = 0;
        $headers = ['Hari','Tanggal','Tahun Ajaran','Semester','Jam Datang','Menit Kesiangan','Keterangan'];
        foreach ($absences as $absence){
            $sumMenit += $absence->menit_kesiangan;
        }
        return view('laporan.show',compact('siswa','siswa_tingkat','absences','sumMenit','headers'));
    }

    public function exportLaporan(Request $request){
        $input = $request->all();
        if($input['kd_tahun_ajaran']!='')
            $input['kd_tahun_ajaran'] = \DB::table('t_tahun_ajaran')->where('tahun_ajaran',$input['kd_tahun_ajaran'])->first()->kd_tahun_ajaran;
        if($input['kd_periode_belajar']=='Ganjil')
            $input['kd_periode_belajar']='1';
        else if($input['kd_periode_belajar']=='Genap')
            $input['kd_periode_belajar']='2';
        else
            $input['kd_periode_belajar']='';

        $headers = ['NIS','Nama','Kelas','Tahun Ajaran','Periode Belajar','Hari/Tanggal','Jam Datang','Menit Kesiangan','Piket','Keterangan'];
        $absences = \DB::table('t_kesiangan')
            ->where('tanggal','like',$input['tgl'].'%')
            ->where('nis','like',$input['nis'].'%')
            ->where('kd_rombel','like',$input['kelas'].'%')
            ->where('kd_tahun_ajaran','like',$input['kd_tahun_ajaran'].'%')
            ->where('kd_periode_belajar','like',$input['kd_periode_belajar'].'%')
            ->get();
        if($input['tgl']=='')
            $tanggal = 'All';
        else
            $tanggal = $input['tgl'];
        if(Input::get('pdf')){
            $pdf = \PDF::loadView('print.layout',compact('headers','absences','tanggal'))->setPaper('a4')->setOrientation('landscape');
            return $pdf->stream();
        }
        elseif(Input::get('excel')){
            $data = compact('headers','absences','tanggal');
            $title ='Kesiangan_'.$data['tanggal'];
            return Excel::create($title,function($excel) use ($data){
                $excel->setTitle('Laporan Keterlambatan Siswa');
                $excel->setCreator('Admin')->setCompany('SMAN 1 Cianjur');
                $excel->sheet('Laporan Siswa Kesiangan', function($sheet) use ($data) {
                    $sheet->loadView('print.table',$data);
                    $sheet->setAutoSize(true);
                });
            })->export('xlsx');
        }
    }
}
