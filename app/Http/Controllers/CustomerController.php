<?php

namespace App\Http\Controllers;

use App\Absence;
use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Jenssegers\Date\Date;

class CustomerController extends Controller
{
    public function getHome(){
        if(Auth::check() && Auth::getUser()->getAttribute('admin')==1){
            return redirect('admin');
        }
        else{
            Date::setLocale('id');
            $user = Auth::getUser();
            $pewangi = \DB::table('pewangi')->select('nama')->get();
            $arrPewangi = array();
            foreach($pewangi as $p){
                $arrPewangi[$p->nama]=$p->nama;
            }
            return view('home',compact('user','arrPewangi'));
        }
    }

    public function postHome(Request $request){
        $this->validate($request,[
            'alamat'=>'required',
            'telp'=>'required',
            'berat'=>'required'
        ]);
        \DB::table('order')->insert([
            'nama'=>$request->name,
            'alamat'=>$request->alamat,
            'no_telp'=>$request->telp,
            'pewangi'=>$request->pewangi,
            'pengerjaan'=>$request->pengerjaan,
            'berat'=>$request->berat,
            'total_bayar'=>$request->bayar,
            'status'=>0,
            'lunas'=>0
        ]);
        return redirect('home');
    }

    public function getConfig(){
        $piket = \DB::table('t_piket')->orderBy('id','asc')->get();
        $guru = \DB::table('t_guru')->where('status','1')->orderBy('nm')->get();
        return view('absences.config',compact('piket','guru'));
    }

    public function postConfig(Request $request){
        $input = $request->all();
        for($i=1;$i<7;$i++){
            \DB::table('t_piket')
                ->where('id',$i)
                ->update([
                    'jam_masuk'=>\Input::get('time'.$i),
                    'kd_guru'=>\Input::get('guru'.$i)
                ]);
        }
        return redirect('/home/configuration');
    }
}
