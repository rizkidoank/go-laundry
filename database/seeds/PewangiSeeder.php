<?php

use Illuminate\Database\Seeder;

class PewangiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pewangi = ['Lavender','Downy','Strawberry','Molto','Daia'];
        foreach($pewangi as $p){
            DB::table('pewangi')->insert([
                'nama'=>$p
            ]);
        }
    }
}
