@extends('layout.master')

@section('content')
    @include('additional.navbar')
    <div class="container">
        <div class="col-md-3">
            @if(Session::has('login_flash'))
                <div class="alert alert-success" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Notification</strong><br/>
                        {{Session::get('login_flash')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Notification</strong><br/>
                    {{Session::get('error')}}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Notification</strong><br/>
                    {{Session::get('success')}}
                </div>
            @endif
        </div>
        <div class="col-md-10 col-md-offset-1">
            <div id="add-panel" class="panel panel-default animated fadeInDown">
                <div class="panel-heading">
                    <h3 class="panel-title">Go-Laundry</h3>
                </div>
                <div class="panel-body">
                    {!! Form::open(['class'=>'form-horizontal']) !!}
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-4">
                                {!! Form::label('nama','Nama',['class'=>'control-label']) !!}
                            </div>
                            <div class="col-md-8">
                                {!! Form::text('name',$user->name,['class'=>'form-control','readonly']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                {!! Form::label('alamat','Alamat',['class'=>'control-label']) !!}
                            </div>
                            <div class="col-md-8">
                                {!! Form::text('alamat','',['class'=>'form-control','autocomplete'=>'off']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                {!! Form::label('telp','No.Telp',['class'=>'control-label']) !!}
                            </div>
                            <div class="col-md-8">
                                {!! Form::text('telp','',['class'=>'form-control','autocomplete'=>'off']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                {!! Form::label('pewangi','Pewangi',['class'=>'control-label']) !!}
                            </div>
                            <div class="col-md-8">
                                {!! Form::select('pewangi',$arrPewangi,'',['class'=>'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-4">
                                {!! Form::label('pengerjaan','Pengerjaan (hari)',['class'=>'control-label']) !!}
                            </div>
                            <div class="col-md-8">
                                {!! Form::number('pengerjaan',2,['class'=>'form-control','readonly']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                {!! Form::label('berat','Berat (Kg)',['class'=>'control-label']) !!}
                            </div>
                            <div class="col-md-8">
                                {!! Form::text('berat','',['class'=>'form-control','autocomplete'=>'off']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                {!! Form::label('bayar','Total Bayar',['class'=>'control-label']) !!}
                            </div>
                            <div class="col-md-8">
                                {!! Form::text('bayar','',['class'=>'form-control','autocomplete'=>'off','readonly']) !!}
                            </div>
                        </div>
                    </div>
                    {!! Form::submit('Order',['class'=>'form-control btn btn-success']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script !src="">
        $(document).ready(function(){
            $("#berat").keyup(function(){
                $("#bayar").val(7000*Math.round($("#berat").val()));
            });
        });
    </script>
@stop