@extends('layout.master')
@section('content')
    @include('additional.navbar')
    <div class="container">
        <div id="laporan-panel" class="panel panel-default animated fadeInLeft">
            <div class="panel-heading">
                <h3 class="panel-title">Detail Order</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    {!! Form::open() !!}
                    <div class="form-group col-sm-10">
                        {!! Form::text('nama',null,['class'=>'form-control','id'=>'nama','placeholder'=>'nama']) !!}
                    </div>
                    <div class="form-group col-sm-1">
                        {!! Form::submit('PDF',['class'=>'btn btn-primary form-control','name'=>'pdf']) !!}
                    </div>
                    <div class="form-group col-sm-1">
                        {!! Form::submit('Excel',['class'=>'btn btn-primary form-control','name'=>'excel']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="table-responsive">
                    <table class="table table-hover dt-responsive" id="tabel">
                        <thead>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>No Telp</th>
                            <th>Pewangi</th>
                            <th>Pengerjaan</th>
                            <th>Berat</th>
                            <th>Total Bayar</th>
                            <th>Status</th>
                            <th>Lunas</th>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td><a href="/home/info/{{$order->nama}}">{{$order->nama}}</a></td>
                                <td>{{$order->alamat}}</td>
                                <td>{{$order->no_telp}}</td>
                                <td>{{$order->pewangi}}</td>
                                <td>{{$order->pengerjaan}} hari</td>
                                <td>{{$order->berat}} Kg</td>
                                <td>Rp {{$order->total_bayar}}</td>
                                @if($order->status==1)
                                    <td>Selesai</td>
                                @else
                                    <td>
                                        {!! Form::open(array('url'=>'admin/updateStatus')) !!}
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button  type="submit" class="btn-sm btn-warning">Update</button>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </td>
                                @endif
                                @if($order->lunas==1)
                                    <td>lunas</td>
                                @else
                                    <td>
                                        {!! Form::open(array('url'=>'admin/updateStatus')) !!}
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button  type="submit" class="btn-sm btn-warning">Update</button>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </td>
                                @endif
                                <td>{{$order->lunas}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script !src="">
        $('document').ready(function(){
            var a=$("#tabel").DataTable({
                dom: 't'
            });
            $("#nama").on("keyup change",function(){
                a.column(0).search($("#nama").val()).draw()
            });
        });
    </script>
@stop